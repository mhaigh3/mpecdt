#!/usr/bin/python

# Outer code for setting up the linear advection problem on a uniform 
# grid and calling the function to perform the linear advection and
# plot. The function takes an argument which gives the name of the 
# input file.

from __future__ import absolute_import, division, print_function

import sys

# Read in all the linear advection schemes, initial conditions and
# other code associated with this application.

from grid import *
from initialConditions import *
from advectionSchemes import *
from diagnostics import *

def main(argv):
	if len(sys.argv) != 2:
		print ('usage: linearAdvect <inputFile-BTCS.py>')
		sys.exit(2)
	inputFile = sys.argv[1]
	print('Reading input from file ', inputFile)

	# Define the input variables from the input file and 
	# store in dictionary inputs.
	inputs = {}
	execfile(inputFile, inputs)

	print('Advecting the profile...')

	grid = Grid(inputs['nx'], inputs['xmin'], inputs['xmax'])

	# Other input variables
	c = inputs['c']		# Courant number for the advection
	
	# Select which initial conditions to use
	initialConditions = eval(inputs['initialConditions'])

	
	scheme = raw_input("Scheme?")
	
	# Intialise the error
	l2errors = []

	tmax = 1000
	stepsize = 1
	timestep = range(1, tmax+stepsize, stepsize)

	for t in timestep:
		# Initialise dependent variable
		phiOld = initialConditions(grid.x)

		if scheme == "CTCS":
			phi = CTCS(phiOld.copy(), c, t)
		elif scheme == "FTBS":
			phi = FTBS(phiOld.copy(), c, t)
		elif scheme == "FTCS":
			phi = FTCS(phiOld.copy(), c, t)
		elif scheme == "BTCS":
			phi = BTCS(phiOld.copy(), c, t)	 
		else:
			print ("Not a scheme")
			return None


		# Exact solution is the initial condition shifted around the domain.
		phiExact = initialConditions((grid.x - c*t*grid.dx)%grid.length)

		error = errorDiagnostics(grid, phi, phiExact)

		l2errors.append(error.l2())

	plotFinal(timestep, [l2errors], [str(scheme) + ' error'], inputs['outFile'])



### Run the function main defined in this file ###
if __name__ == "__main__":
   main(sys.argv[1:])



# Numerical schemes for simulating linear advection for outer code
# linearAdvect.py 

from __future__ import absolute_import, division, print_function

import numpy as np

from diagnostics import *

#Linear advection of profile in phiOld using FTBS, Courant numberc 	#for nt time-steps"

def CTCS(phiOld, c, nt):
	nx = len(phiOld)

	# plot results as we go along
	plotSolution(phiOld, "Initial Conditions")

	# new time-step array for phi
	phi = np.zeros_like(phiOld)
 
	#CTCS for each time-step
	for it in range(nt):
		#Loop through all space using remainder after division 
		# to cope with periodic boundary conditions
		for j in range(nx):
			phi[j] = phiOld[j] - \
						c*(phiOld[(j)%nx] - phiOld[(j-1)%nx])

		# update arrays for next time-step
		phiOld = phi.copy()	
	
		#plot for this time-step
		plotSolution(phi, "FTBS, time step "+str(it))

	return phi



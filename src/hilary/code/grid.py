# Data structure for the grid for the linear advection

from __future__ import absolute_import, division, print_function

import numpy as np

class Grid(object):
	# Store all grid date and calculates dx and x locations
	# The grid is assumed periodic
	def __init__(self, nx, xmin=0.0, xmax=1.0):
		self.xmin = xmin
		self.xmax = xmax
		self.nx = nx
		self.length = float(self.xmax - self.xmin)
		self.dx = self.length/self.nx
		# The x locations excluding the end point
		self.x = np.arange(self.xmin, self.xmax, self.dx)


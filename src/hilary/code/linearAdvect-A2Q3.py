#!/usr/bin/python

# Outer code for setting up the linear advection problem on a uniform 
# grid and calling the function to perform the linear advection and
# plot. The function takes an argument which gives the name of the 
# input file.

from __future__ import absolute_import, division, print_function

import sys

# Read in all the linear advection schemes, initial conditions and
# other code associated with this application.

from grid import *
from initialConditions import *
from advectionSchemes import *
from diagnostics import *

def main(argv):
	if len(sys.argv) != 2:
		print ('usage: linearAdvect <inputFile-BTCS.py>')
		sys.exit(2)
	inputFile = sys.argv[1]
	print('Reading input from file ', inputFile)

	# Define the input variables from the input file and 
	# store in dictionary inputs.
	inputs = {}
	execfile(inputFile, inputs)

	print('Advecting the profile...')

	# Other input variables
	c = inputs['c']		# Courant number for the advection
	
	# Select which initial conditions to use
	initialConditions = eval(inputs['initialConditions'])

	
	scheme = raw_input("Scheme?")
	
	# An empty list to store the errors.
	l2errors = []

	max_x = 100
	res = 20
	
	
	spacestep = range(1, max_x+res, res)

	for nx in spacestep:

		grid = Grid(nx, inputs['xmin'], inputs['xmax'])
		

		# Initialise dependent variable			
		phiOld = initialConditions(grid.x)

		nt = int(c*nx)

		if scheme == "CTCS":
			phi = CTCS(phiOld.copy(), c, nt)
		elif scheme == "FTBS":
			phi = FTBS(phiOld.copy(), c, nt)
		elif scheme == "FTCS":
			phi = FTCS(phiOld.copy(), c, nt)
		elif scheme == "BTCS":
			phi = BTCS(phiOld.copy(), c, nt)	 
		else:
			print ("Not a scheme")
			return None
		
		# Exact solution is the initial condition shifted around the domain.
		phiExact = initialConditions((grid.x - c*nt*grid.dx)%grid.length)


		error = errorDiagnostics(grid, phi, phiExact)

		l2errors.append(error.l2())

	print (spacestep, l2errors)

	plotFinal(spacestep, [l2errors], ['error'], inputs['outFile'])



### Run the function main defined in this file ###
if __name__ == "__main__":
   main(sys.argv[1:])



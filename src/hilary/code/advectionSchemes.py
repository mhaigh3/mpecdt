# Numerical schemes for simulating linear advection for outer code
# linearAdvect-FTCS.py linearAdvect-FTBS.py, linearAdvect-CTCS.py 
# and linearAdvect-BTCS.py 

from __future__ import absolute_import, division, print_function

import numpy as np

from diagnostics import *

import scipy.linalg as la

#Linear advection of profile in phiOld using FTCS, FTBS, CTCS, BTCS
# with Courant number c for nt time-steps


"Forward in time centered in space advection sheme"
def FTCS(phiOld, c, nt):

	nx = len(phiOld)
	
	# plot results as we go along
	# plotSolution(phiOld, "Initial Conditions")

	# new time-step array for phi
	phi = np.zeros_like(phiOld)
	

	#FTCS for each time-step
	for it in range(nt):
		# Loop through all space using remainder after division
		# to cope with periodic boundary conditions
		for j in range(nx):
			phi[j] = phiOld[j] - \
				0.5*c*(phiOld[(j+1)%nx] - phiOld[(j-1)%nx])

		# update arrays for next time-step
		phiOld = phi.copy()	
	
		#plot for this time-step
		# plotSolution(phi, "FTCS, time step "+str(it))

	return phi


"Forward in time backward in space advection sheme"
def FTBS(phiOld, c, nt):

	nx = len(phiOld)

	# plot results as we go along
	# plotSolution(phiOld, "Initial Conditions")

	# new time-step array for phi
	phi = np.zeros_like(phiOld)
 
	#FTBS for each time-step
	for it in range(nt):
		# Loop through all space using remainder after division
		# to cope with periodic boundary conditions
		for j in range(nx):
			phi[j] = phiOld[j] - \
				c*(phiOld[(j)%nx] - phiOld[(j-1)%nx])

		# update arrays for next time-step
		phiOld = phi.copy()	
	
		#plot for this time-step
		# plotSolution(phi, "FTBS, time step "+str(it))

	return phi


"Centered in time centered in space advection sheme"
def CTCS(phiOld, c, nt):	

	nx = len(phiOld)

	# plot results as we go along
	# plotSolution(phiOld, "Initial Conditions")

	# Defining the second initial condition via FTCS
	phi = FTCS(phiOld, c, 1)

	phiNew = np.zeros_like(phiOld)

	#CTCS for each time-step
	for it in range(nt-1):
		# Loop through all space using remainder after division
		# to cope with periodic boundary conditions
		for j in range(nx):
			phiNew[j] = phiOld[j] - \
				c*(phi[(j+1)%nx] - phi[(j-1)%nx])

		# update arrays for next time-step
		phiOld = phi.copy()	
		phi = phiNew.copy()
	
		# plot for this time-step
		# plotSolution(phi, "CTCS, time step "+str(it))

	return phi

"Backward in time centered in space advection sheme"
def BTCS(phiOld, c, nt):

	nx = len(phiOld)

	# plot results as we go along
	# plotSolution(phiOld, "Initial Conditions")
	
	# new time-step array for phi and phiNew
	phi = phiOld

	# Find each time-step of phi by solving the matrix equation at 		
	# each time-step.

	# Create an array of zeros
	M = np.zeros([nx,nx])

	# Prescribe the values of the matrix in the top and bottom rows
	M[0,0], M[0,1], M[0,nx-1] = 1, c/2, -c/2
	M[nx-1,0], M[nx-1,nx-2], M[nx-1, nx-1] = c/2, -c/2, 1

	# A loop to presribe all other values within the matrix
	for i in range(1,nx-1):											
		M[i,i-1] = -c/2
		M[i,i] = 1
		M[i,i+1] = c/2

	#BTCS for each time-step
	for it in range(nt):

		# Find phi by solving the matrix problem using linalg package
		phi = la.solve(M, phi)

		#plot for this time-step
		# plotSolution(phi, "BTCS, time step "+str(it))

	return phi



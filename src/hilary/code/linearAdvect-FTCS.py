#!/usr/bin/python

# Outer code for setting up the linear advection problem on a uniform 
# grid and calling the function to perform the linear advection and
# plot. The function takes an argument which gives the name of the 
# input file.

from __future__ import absolute_import, division, print_function

import sys

# Read in all the linear advection schemes, initial conditions and
# other code associated with this application.

from grid import *
from initialConditions import *
from advectionSchemes import *
from diagnostics import *

def main(argv):
	if len(sys.argv) != 2:
		print ('usage: linearAdvect <inputFile-FTCS.py>')
		sys.exit(2)
	inputFile = sys.argv[1]
	print('Reading input from file ', inputFile)

	# Define the input variables from the input file and 
	# store in dictionary inputs.
	inputs = {}
	execfile(inputFile, inputs)

	print('Advecting the profile...')

	grid = Grid(inputs['nx'], inputs['xmin'], inputs['xmax'])

	# Other input variables
	c = inputs['c']		# Courant number for the advection
	nt = inputs['nt']	# Total number of time steps to run for
	# Select which initial conditions to use
	initialConditions = eval(inputs['initialConditions'])

	# Initialise dependent variable
	phiOld = initialConditions(grid.x)
	# Exact solution is the initial condition shifted around the 		# domain.
	phiExact = initialConditions((grid.x - c*nt*grid.dx)%grid.length)

	# Advect the profile using finite difference for all the time steps
	phiFTCS = FTCS(phiOld.copy(), c, nt)

	#Calculate the error norms, mean and standard deviation
	noErrors = errorDiagnostics(grid, phiExact, phiExact)
	noErrors.write("Exact")
	FTCSerrors = errorDiagnostics(grid, phiFTCS, phiExact)
	FTCSerrors.write("FTCS")

	plotFinal(grid.x, [phiExact, phiFTCS], ['Exact Solution', 'FTCS'], inputs['outFile'])

### Run the function main defined in this file                      ###
if __name__ == "__main__":
   main(sys.argv[1:])



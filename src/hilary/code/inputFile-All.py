# Input variables for the linear advection

c = 0.2                     # Courant number for the advection
nt = 40                   # Total number of time steps to run for
nx = 100                    # Number of spatial points
xmin = 0.                   # Limits of the spatial domain, for plotting
xmax = 1.
initialConditions = 'IC2' # Define which initial conditions to use
outFile = "All_c02_nt100_nx100.pdf"

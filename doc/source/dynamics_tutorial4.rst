Computational Lab: Dynamics 4
=============================

This is the tutorial material for Computational Lab 4 in the Dynamics
core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpcdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about invariant measures and
ergodicity. 

.. container:: tasks  

   After merging the latest version of the mpecdt repository, you will
   find a file to the mpecdt/src directory called
   `sde_convergence.py`, in which you should add all of the tasks from
   this tutorial.  

In this computer lab we will consider stochastic differential
equations (SDEs) of the form 

.. math::
   dx = a(x,t)dt + b(x,t)dW,
   :label: SDE 

where :math:`a(x,t)` and :math:`b(x,t)` are given functions. This notation
is shorthand for the integral formulation

.. math:: \int_{t_0}^{t_1} dx(t) = \int_{t_0}^{t_1}a(x(t),t)dt +
   \int_{t_0}^{t_1} b(x(t),t)dW(t),

where :math:`W(t)` is a Brownian motion, a random variable satisfying

#. :math:`W(0) = 0` 
#. :math:`W(t_1)-W(t_2)` is statistically independent of :math:`W(t_3)-W(t_4)`
   for :math:`t_4<t_3<t_2<t_1`.
#. :math:`W(t)-W(s)\sim N(0,t-s)` for :math:`0\leq s \leq t`.

A formal definition is required for the stochastic integral containing
:math:`dW`, for now we just define our SDE as the limit of the
following discrete random process as :math:`\Delta t \to 0`,

.. math::
   
   x^{n+1} = x^n + a(x^n,n\Delta t)\Delta t + b(x^n,n\Delta t)\Delta W,

where :math:`\Delta W \sim N(0,\Delta t)`. For fixed :math:`\Delta t`, this
discrete process is the most basic numerical approximation to our SDE,
known as the Euler-Maruyama approximation to Equation :eq:`SDE`.

In atmosphere, ocean and weather applications, stochastic differential
equations are used to model unresolved effects, unknown model errors,
and uncertainties in parameters and data.

Exercise 1
----------

The file `sde_convergence.py` implements the Euler-Maruyama method for
the 1-dimensional SDE with :math:`a(x,t)=-x`, :math:`b(x,t)=1`, i.e. an
Ornstein-Uhlenbeck process, and plots the result.

.. container:: tasks  

   Execute the file a few times to see different realisations of this
   stochastic process.

Exercise 2
----------

We will investigate the convergence of this approximation. In the case

.. math:: 
   
   a(x,t)=cx, \quad b(x,t)=ex,

we have the exact solution,

.. math::

   x(t) = X(0)\exp\left(\left(c-\frac{1}{2}e^2\right)t + eW(t)\right).

.. container:: tasks  

   The code provided returns arrays for three variables, `x` (the
   solution of the SDE), `t` (the time), and `W` (the Brownian motion
   used to generate the process). Plot `x` and `W` on the same axes.
   Are they close? What is happening to the error between the exact
   solution and numerical solution?

Exercise 3
----------

Now we will quantify the error using various error metrics. The :math:`L_p`
norm is

.. math::
   
   \epsilon_{L_p} = \left|\mathbb{E}\left[
   (x^n - x(t^n))^p
   \right]\right|^{1/p},

where :math:`x^n` is the numerical solution at time :math:`t^n` and
:math:`x(t)` is the exact solution. If the :math:`L_p` norm converges
to zero as :math:`\Delta t \to 0`, then we say that the numerical
method exhibits "strong convergence" to the exact solution. We say that 
a numerical method is "strong order :math:`\alpha`" if

.. math::
   \epsilon_{L_p} = \mathcal{O}(\Delta t^\alpha),

i.e.,

.. math::
   \lim_{\Delta \to 0}\frac{\epsilon_{L_p}}{\Delta t^\alpha}=c_0,

for some constant :math:`c_0`.

.. container:: tasks

   Using the exact solution for the special case given above, with
   :math:`c=d=1`, evaluate the :math:`L_1` norm of the error at time
   :math:`t=1`, for various values of :math:`\Delta t`. This can be
   done by approximating the expectation using a Monte Carlo sum over
   several evaluations of the quantity inside the expectation.

   Plot the :math:`L_1` norm against :math:`\Delta t` on a log-log
   graph, and compare the slope to different powers of :math:`\Delta
   t`. What is the strong order of convergence of the Euler-Maruyama
   method?

.. container:: tasks

   Repeat the above exercise using the :math:`L_2` norm.

Exercise 4
----------

The :math:`L_p` norm measures how close the numerical solution is to
the exact solution for given realisations of the noise W. In many
applications, such as probabilistic weather prediction, we are only
interested in predicting statistics from PDFs. In particular, we are
interested in predicting expectations,

.. math::
   \mathbb{E}\left[f(x)\right],

for suitable functions :math:`f`. Therefore, we can define a weak
error metric,

.. math::
   \epsilon_f = \left|
   \mathbb{E}\left[f(x^n)\right]-
   \mathbb{E}\left[f(x(t^n))\right]
   \right|.

If :math:`\epsilon_f` converges to zero as :math:`\Delta t \to 0` for
all suitable functions :math:`f` (i.e. functions that are measurable
with respect to the probability measure for :math:`x(t^n)`), then 
then we say that the numerical
method exhibits "weak convergence" to the exact solution. We say that 
a numerical method is "weak order :math:`\alpha`" if

.. math::
   \epsilon_{f} = \mathcal{O}(\Delta t^\alpha).

.. container:: tasks

   The exact solution to the Ornstein-Uhlenbeck process from Exercise
   1 has expectation

   .. math::

      \mathbb{E}\left[
      x(t) 
      \right] = x(0)\exp(-t).

   Use this formula to determine the rate of weak convergence for
   :math:`\mathbb{E}[x(1)]`. 

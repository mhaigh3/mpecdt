`Back to Numerics Course Home Page <http://mpecdt.bitbucket.org/>`_

Assignments for Part I of the Numerics Course (Hilary Weller)
=============================================================

* Assignment 1 (week 1): `assig1.pdf
  <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/assig1.pdf>`_
  Deadline 12 noon on Wed 14 October

* Assignment 2 (week 2): `assig2.pdf
  <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/assig2.pdf>`_
  Deadline 12 noon on Wed 28 October

* Assignment 3: `assig3.pdf
  <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/assig3.pdf>`_
  Deadline 12 noon on Fri 13 November


`Back to Numerics Course Home Page <http://mpecdt.bitbucket.org/>`_

Videos for Part I of the Numerics Course (Hilary Weller)
========================================================

Week 1 (5-9 Oct 2015) Introduction and and Linear Advection
-----------------------------------------------------------

.. raw:: html

    <a href="#" onClick="MyWindow=window.open('https://www.youtube.com/watch?v=gnqFGE5G9rY','MyWindow','width=900,height=600'); return false;"> Video 1: Introduction</a><br>

    <a href="#" onClick="MyWindow=window.open('https://www.youtube.com/watch?v=J9dJ8hkacS4','MyWindow','width=900,height=600'); return false;"> Video 2: Atmospheric Dynamics</a><br>

    <a href="#" onClick="MyWindow=window.open('https://www.youtube.com/watch?v=qLOEghXjXTg','MyWindow','width=900,height=600'); return false;"> Video 3: Linear Advection</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/nfi12uod0y8','MyWindow','width=900,height=600'); return false;"> Video 3: Fourier Analysis</a><br>



